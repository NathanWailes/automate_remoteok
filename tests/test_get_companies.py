
import unittest

from autoapply_to_remoteok_companies.get_companies import _get_the_job_postings_currently_on_remoteok


class TestGetCompaniesFromRemoteOk(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestGetCompaniesFromRemoteOk, cls).setUpClass()
        cls.list_of_job_posting_dicts = _get_the_job_postings_currently_on_remoteok()

    def tearDown(self):
        pass

    def test_list_of_jobs_is_not_None(self):
        assert(self.list_of_job_posting_dicts is not None)

    def test_list_of_jobs_is_a_list(self):
        assert(isinstance(self.list_of_job_posting_dicts, list))

    def test_list_of_jobs_length_greater_than_zero(self):
        assert(len(self.list_of_job_posting_dicts) > 0)


if __name__ == '__main__':
    unittest.main()
