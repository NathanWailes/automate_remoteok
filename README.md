#What's this?

 - At the moment this project is an attempt to automatically email every company which has a job show up on RemoteOK.io.
 - At the moment it's set up to work with two cron jobs:
  - One cron job calls a function which:
    1. Queries RemoteOK to see which companies currently have a job listed there.
    1. Compares the names of

#Setup
 
 - Python version used: 3.5
 - You'll need to create a few files in the 'autoapply_to_remoteok_companies' folder:
     - companies\_i\_am\_going\_to\_apply\_to.csv
     - companies\_i\_have\_applied\_to.csv
     - user\_config.json

####The CSV files
Both files should look like this:
> company,website_base_url,email

There should be a blank line underneath the header row in each file.

####user\_config.json
Here's an example of the information you'll want to provide:

    {"email_settings":{
      "username": "your_username@whatever.com",
      "password": "your_password",
      "list_of_to_recipient_email_addresses": [optional_additional_recipients],
      "list_of_cc_recipient_email_addresses": [optional_additional_recipients],
      "list_of_bcc_recipient_email_addresses": ["your_hubspot_bcc_email_tracker_email_address"],
      "subject": "Application for a developer position",
      "error_email_recipients": ["your_email_address"],
      "message": ["Hello,\n",
                  "\n",
                  "This message is broken up into comma-separated lines to make it more readable ",
                  "in the JSON format. The lines will be joined together and treated as a single ",
                  "string.\n",
                  "\n",
                  "FirstName LastName\n",
                  "www.your_website.com\n",
                  "your_phone_number"],
      "list_of_attachments": ["Resume.pdf",
                              "Accomplishments.pdf",
                              "Answers to Frequently Asked Questions.pdf",
                              "Continued Study.pdf",
                              "Portfolio.pdf",
                              "References and Praise.pdf"],
      "server": "smtp.gmail.com",
      "port": 587,
      "is_tls": true
     },
     "api_keys": {
       "google_search": "o9ghiOh7ILG7GOLiuYIykbIBy",
       "email_hunter": "h4p98hh4p98hef4p8he4fph8e4p"
     },
     "misc_info": {
       "google_custom_search_engine_id": "1234567890987654321:og87192eo87"
     }
    }

#Known issues

####Getting CSVs from Excel
If you save a CSV from Excel and try to use that as one of the CSVs, you may get an error because Excel does not use UTF-8 encoding:
> UnicodeDecodeError: 'utf-8' codec can't decode byte 0xa0 in position 3307: invalid start byte

To resolve this, just open the CSV in Sublime Text, select "Save with Encoding" and then select "UTF-8".