""" I originally brought in this code so I could wrap my API calls and not repeatedly query for the same things when
debugging my application.
Source: http://threebean.org/blog/2011/06/08/cached-function-calls-with-expiration-in-python-with-shelve-and-decorator/
"""
import shelve
from _md5 import md5
import datetime
import decorator

from autoapply_to_remoteok_companies.utils import create_directory_if_necessary


def cache_with_the_shelve_module(cache_file, expiry):
    """ Decorator setup
    :param cache_file:
    :param expiry:
    :return:
    """

    def cache_closure(func, *args, **kw):
        """ The actual decorator
        :param func:
        :param args:
        :param kw:
        :return:
        """
        key = md5(':'.join([func.__name__, str(args), str(kw)]).encode('utf-8')).hexdigest()
        create_directory_if_necessary(cache_file)
        d = shelve.open(cache_file)

        # Expire old data if we have to
        if key in d:
            if d[key]['expires_on'] < datetime.datetime.now():
                del d[key]

        # Get new data if we have to
        if key not in d:
            data = func(*args, **kw)
            d[key] = {
                'expires_on' : datetime.datetime.now() + expiry,
                'data': data,
            }

        # Return what we got
        result = d[key]['data']
        d.close()

        return result

    return decorator.decorator(cache_closure)
