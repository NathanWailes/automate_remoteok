from datetime import datetime as dt, timedelta


class Memoize:
    """ Check out this wiki article for information: https://en.wikipedia.org/wiki/Memoization
    If you add '@Memoize' to the line above a function declaration, the results of that function will be saved for
    each input. This is really useful if, for example, you want to avoid making the same call many times to an API.
    Example: I call 'get_auc_for_model()' which in turn calls 'get_model_stats()', which queries the Keystone server to
    get model stats. I then call 'get_amount_weighted_auc_for_model()' which also calls 'get_model_stats()', but because
    I added the '@Memoize' decorator, it doesn't need to query Keystone again for the same customer/dataset/model, b/c
    it saved the results from the identical query it made before.
    """
    def __init__(self, f):
        self.f = f
        self.memo = {}

    def __call__(self, *args):
        if args not in self.memo:
            self.memo[args] = self.f(*args)
        return self.memo[args]


def memoize_the_result_for_x_minutes(minutes=1):
    """
    See unutbu's answer on stackoverflow.com/questions/7492068/ for why I decided to do it this way.
    This implementation is ugly, I should see if I can come up with a cleaner way of doing this.
    :param minutes: int, float
    :return: types.FunctionType (?)

    >>> from datetime import datetime as dt
    >>> @memoize_the_result_for_x_minutes(1)
    ... def function_to_memoize():
    ...     return dt.now()
    >>> value_from_first_call_to_function = function_to_memoize()
    >>> value_from_second_call_to_function = function_to_memoize()
    >>> value_from_first_call_to_function == value_from_second_call_to_function
    True
    """
    def _memoize_the_result_for_x_minutes(function):
        return _MemoizeTheResultForXMinutes(function, minutes)
    return _memoize_the_result_for_x_minutes


class _MemoizeTheResultForXMinutes:
    """ Don't use this class directly; use the function memoize_the_result_for_x_minutes instead.
    I need this function to remember the result, because just implementing the decorator as a function won't let me
    remember values. But I can't implement this decorator as solely a class either because apparently class decorators
    aren't set up to take arguments.
    This implementation is ugly, I should see if I can come up with a cleaner way of doing this.
    """
    def __init__(self, f, minutes_to_remember=1):
        """
        :param f: types.FunctionType (?)
        :param minutes_to_remember: int, float
        :return: None (?)
        """
        self.f = f
        self.memo = {}
        self.minutes_to_remember = minutes_to_remember

    def __call__(self, *args):
        """
        :param args: various? list?
        :return: various?
        """
        if args not in self.memo:
            self.memo[args] = {'function_call_results': self.f(*args),
                               'datetime_called': dt.now()}
        else:
            datetime_that_the_function_was_first_called = self.memo[args]['datetime_called']
            if dt.now() - datetime_that_the_function_was_first_called > timedelta(minutes=self.minutes_to_remember):
                self.memo[args] = {'function_call_results': self.f(*args),
                                   'datetime_called': dt.now()}
        return self.memo[args]['function_call_results']