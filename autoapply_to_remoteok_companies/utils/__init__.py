import csv
import logging
import os
import traceback
from datetime import datetime
from pkg_resources import resource_filename
from functools import wraps
from autoapply_to_remoteok_companies import send_an_email  # To avoid a circular import I can't import exact functions from this module. SO.com/Qs/7336802/


def run_with_logging(function):
    @wraps(function)
    def wrapped(*args, **kwargs):
        path_to_log_file = resource_filename('autoapply_to_remoteok_companies', 'logs/' + function.__name__ + '.log')
        create_directory_if_necessary(path_to_log_file)
        logging.basicConfig(filename=path_to_log_file, level=logging.DEBUG)
        logging.info('Starting at ' + str(datetime.now()))
        try:
            function(*args, **kwargs)
        except Exception as e:
            logging.exception(e)
            traceback_message = traceback.format_exc()
            send_an_email.send_email_with_error_message(traceback_message)
        logging.info('Finished at ' + str(datetime.now()))
    return wrapped


def create_directory_if_necessary(path_to_file_or_directory):
    """ Code from http://stackoverflow.com/a/23704144/4115031

    2017.11.28 - This function had a leading underscore when I created it back in January, and I don't remember why. I
    just took it off, but it may have been to warn my future self about something important.

    :param path_to_directory:
    :return:
    """
    path_to_directory = os.path.dirname(path_to_file_or_directory)
    if path_to_directory and not os.path.exists(path_to_directory):
        original_umask = os.umask(0)
        try:
            desired_permission = 777
            os.makedirs(path_to_directory, desired_permission)
        finally:
            os.umask(original_umask)


def value_in_column_of_csv(value, column_name, path_to_csv):
    """
    :param value:
    :param column_name:
    :param path_to_csv:
    :return:
    """
    with open(path_to_csv, 'r') as infile:
        reader = csv.DictReader(infile)
        values_in_csv = set([row.get(column_name) for row in reader])
    return value in values_in_csv


def name_is_in_column_of_csv(name_to_look_for, column_name, path_to_csv):
    """ This function is to handle cases like "big-cartel" vs. "Big Cartel"
    :param name_to_look_for:
    :param column_name:
    :param path_to_csv:
    :return:
    """
    with open(path_to_csv, 'r') as infile:
        reader = csv.DictReader(infile)
        normalized_names_in_csv = set([_get_normalized_name(row.get(column_name, '')) for row in reader])
    name_with_only_letters_and_numbers = _get_normalized_name(name_to_look_for)
    return name_with_only_letters_and_numbers in normalized_names_in_csv


def _get_normalized_name(string):
    """ TODO: Come up with a better name for this function.
    :param string:
    :return:
    """
    return ''.join([character for character in string if character.isalnum()]).lower()
