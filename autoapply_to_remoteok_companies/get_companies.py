""" Original full name of this file: get_companies_that_i_havent_applied_to_that_have_posted_a_job
TODO: Deal with the situation of not having any remaining API calls from Email Hunter or Google.
TODO: If Email Hunter crashes, all of the queries I did to Google get lost. Save the results of queries I've done to Google.
TODO: Once a day, email me a list of the companies I'm going to be emailing.

My idea is to use this module to save companies to a CSV, then email the contents of that CSV to myself once a day,
give the email a once-over to make sure it looks OK, and then .
"""
import csv
from queue import PriorityQueue

from pkg_resources import resource_filename
import requests
from autoapply_to_remoteok_companies.apis.email_hunter_wrapper import exist, search
from autoapply_to_remoteok_companies.company import Company, append_companies_to_csv, _get_the_company_domain_given_its_url, \
    find_the_company_domain_on_google_given_the_name
from autoapply_to_remoteok_companies.utils import run_with_logging, value_in_column_of_csv, name_is_in_column_of_csv
from autoapply_to_remoteok_companies.utils.memoize import memoize_the_result_for_x_minutes
import sys

# The line below is to avoid creating .pyc files, which can get 'stale': you can update a repository, run the code, and
# it will use the .pyc file, which will still be using the old version of the code, instead of the .py file, which will
# have any new changes you've made.
sys.dont_write_bytecode = True


@run_with_logging
def get_and_save_companies_i_want_to_apply_to(path_to_csv_of_companies_i_have_applied_to,
                                              path_to_csv_of_companies_im_going_to_apply_to,
                                              path_to_csv_of_job_posts_ive_processed):
    """
    :param path_to_csv_of_companies_i_have_applied_to:
    :param path_to_csv_of_companies_im_going_to_apply_to:
    :param path_to_csv_of_job_posts_ive_processed:
    :return:
    """
    all_job_postings_on_remoteok = _get_the_job_postings_currently_on_remoteok()

    companies_i_want_to_add_to_my_csv = _get_companies_i_want_to_add_to_my_csv(all_job_postings_on_remoteok,
                                                                               path_to_csv_of_companies_i_have_applied_to,
                                                                               path_to_csv_of_companies_im_going_to_apply_to,
                                                                               path_to_csv_of_job_posts_ive_processed)
    append_companies_to_csv(companies_i_want_to_add_to_my_csv, path_to_csv_of_companies_im_going_to_apply_to)

    _save_job_posting_ids_to_a_csv(all_job_postings_on_remoteok, path_to_csv_of_job_posts_ive_processed)


def _get_companies_i_want_to_add_to_my_csv(all_job_postings_on_remoteok,  # TODO: Improve the name of this function.
                                           path_to_csv_of_companies_i_have_applied_to,
                                           path_to_csv_of_companies_im_going_to_apply_to,
                                           path_to_csv_of_job_posts_ive_processed):
    """
    This function gets a list of companies from a CSV file.
    :param path_to_csv_of_companies_i_have_applied_to:
    :return:
    """
    unprocessed_job_postings = [post for post in all_job_postings_on_remoteok
                                if not value_in_column_of_csv(post['id'], 'id',
                                                              path_to_csv_of_job_posts_ive_processed)]

    company_names_in_the_job_postings = set([post['company'] for post in unprocessed_job_postings
                                             if _company_name_is_of_interest(post['company'])])

    company_names_not_in_my_csvs = [company_name for company_name in company_names_in_the_job_postings
                                    if not name_is_in_column_of_csv(company_name, 'name',
                                                                    path_to_csv_of_companies_i_have_applied_to) \
                                    and not name_is_in_column_of_csv(company_name, 'name',
                                                                     path_to_csv_of_companies_im_going_to_apply_to)]

    print("Getting domain info...")
    companies_minus_email_info = [Company(name=company_name,
                                          website_base_url=find_the_company_domain_on_google_given_the_name(company_name),  # TODO: Deal with situations where Google returns the wrong website_base_url.
                                          email="") for company_name in company_names_not_in_my_csvs]  # Initializing to a blank email seems dangerous...

    print("Getting email info...")
    companies_filtered_by_base_url = [company for company in companies_minus_email_info
                                      if not value_in_column_of_csv(company.website_base_url, 'website_base_url', path_to_csv_of_companies_i_have_applied_to) \
                                      and not value_in_column_of_csv(company.website_base_url, 'website_base_url',
                                                                     path_to_csv_of_companies_im_going_to_apply_to)
                                      and not _company_website_is_on_my_blacklist(company)]

    companies_including_the_email_addresses = _get_list_of_companies_that_includes_the_email_addresses(companies_filtered_by_base_url)
    return companies_including_the_email_addresses


def _company_name_is_of_interest(company_name):
    """ We don't want to deal with blank company names or those which we won't be able to reliably find
    a URL for by searching Google. len() > 3 is just a quick-and-dirty heuristic for now. See AUTORSPND-9
    :param company_name:
    :return:
    """
    if company_name in ['infer', 'remoteok.io']:
        return False
    elif len(company_name) <= 3:
        return False
    return True


@memoize_the_result_for_x_minutes(1)
def _get_the_job_postings_currently_on_remoteok():
    """
    :return:
    """
    response = requests.get("https://remoteok.io/remote-jobs.json")
    list_of_job_posting_dicts = response.json()
    return list_of_job_posting_dicts


def _company_website_is_on_my_blacklist(company):
    """ Note that as of 2016.10.13 this is run before I have searched for the email addresses.

    :param company:
    :return:
    """
    if company.website_base_url in ['infer.com', 'remoteok.io']:
        return True
    return False


def _get_list_of_companies_that_includes_the_email_addresses(companies_with_the_base_url):
    """

    :param companies_with_the_base_url:
    :return:
    """
    list_of_companies_that_include_the_email_addresses = []

    for index, company in enumerate(companies_with_the_base_url):
        email_address_to_send_my_resume_to = _get_the_email_address_to_send_my_resume_to(company.website_base_url)

        company_including_the_email_address = company._replace(email=email_address_to_send_my_resume_to)  # "_replace" is because namedtuples are immutable

        list_of_companies_that_include_the_email_addresses.append(company_including_the_email_address)

    return list_of_companies_that_include_the_email_addresses


def _get_the_email_address_to_send_my_resume_to(url_of_company):
    """
    TODO: Handle situations where you want to send the resume to multiple people.
    :param url_of_company:
    :return:
    """
    company_domain = _get_the_company_domain_given_its_url(url_of_company)  # TODO: What happens if this doesn't return anything?

    email_address_to_send_my_resume_to = _get_the_best_generic_email_address_to_send_my_resume_to(company_domain)

    if not email_address_to_send_my_resume_to:
        email_address_to_send_my_resume_to = _get_the_best_personal_email_address_to_send_my_resume_to(company_domain)

    if not email_address_to_send_my_resume_to:
        email_address_to_send_my_resume_to = "postmaster@" + company_domain  # TODO: Do something better in this situation.
    return email_address_to_send_my_resume_to


def _get_the_best_generic_email_address_to_send_my_resume_to(company_domain):
    """ The best generic email address _to_send_my_resume_to_.
    :param company_domain:
    :return:
    """
    email_address_to_send_my_resume_to = None
    found_email_addresses = set()

    for result in search(company_domain, type_='generic'):
        found_email_addresses.add(result['value'])

    if found_email_addresses:
        # The reason I need a priority queue is that I only get to look at each email address one-at-a-time,
        # and if (for example) I'm looking at the first email address and it looks 'OK' but not great, I don't know yet
        # if that's the one I'll want to use, because I haven't seen the rest of the email addresses.
        priority_queue = PriorityQueue()
        for email_address in found_email_addresses:
            email_address = email_address.lower()  # This shouldn't be necessary but I'm just being extra-careful.
            if any([email_address.startswith(string) for string in ['jobs@', 'resumes@', 'hiring@', 'careers@']]):
                priority_queue.put((1, email_address))
            elif email_address.startswith('tech@'):
                priority_queue.put((5, email_address))
            elif any([email_address.startswith(string) for string in ['info@', 'contact@']]):
                priority_queue.put((10, email_address))
            elif any([string in email_address for string in ['job', 'resume', 'hiring', 'career']]):
                priority_queue.put((15, email_address))
            elif any([email_address.startswith(string) for string in ['support@']]):
                priority_queue.put((20, email_address))
            else:
                # TODO: In this case, use EmailHunter to check if any of the desired email addresses above accept mail.
                priority_queue.put((99, email_address))  # TODO: This looks like it could cause problems.

        email_address_to_send_my_resume_to = priority_queue.get()[1]  # Take the most-desirable email address.
    #else:  # Pinging these generic email addresses individually at this point could result in a lot of requests to EH...consider first going for a personal email address.
    #    email_address_to_send_my_resume_to = _get_the_best_generic_email_address_via_pinging(company_domain)
    return email_address_to_send_my_resume_to


def _get_the_best_generic_email_address_via_pinging(company_domain):
    """ TODO: Have this use the 'verify' EH endpoint once it's working, don't use 'client.exist'. See explanation below.
    :param company_domain:
    :return:
    """
    for email_address_local_part in ['jobs', 'resumes', 'careers', 'hiring', 'contact', 'info', 'support']:
        candidate_email_address = email_address_local_part + "@" + company_domain
        response = exist(candidate_email_address)  # This doesn't actually ping the email address. You're getting the same info as with the 'search' query.
        email_address_exists = response[0]
        if email_address_exists:
            return candidate_email_address
    return None


def _get_the_best_personal_email_address_to_send_my_resume_to(company_domain):
    """

    :param company_domain:
    :return:
    """
    # TODO: Maybe prefer email addresses that have sources on the actual website. Ex: https://emailhunter.co/search/entrision.com
    personal_email_addresses = search(company_domain, type_='personal')
    email_address_to_send_my_resume_to = personal_email_addresses[0]['value'] if personal_email_addresses else None  # Just take the first one, as a semi-random choice. TODO: Improve this
    return email_address_to_send_my_resume_to


def _save_job_posting_ids_to_a_csv(all_job_postings_on_remoteok, path_to_csv_of_job_posts_ive_processed):
    """

    :param all_job_postings_on_remoteok:
    :param path_to_csv_of_job_posts_ive_processed:
    :return:
    """
    job_post_ids = [post['id'] for post in all_job_postings_on_remoteok]
    with open(path_to_csv_of_job_posts_ive_processed, "a") as outfile:
        fieldnames = ['id']
        writer = csv.DictWriter(outfile, lineterminator='\n', fieldnames=fieldnames)
        for job_post_id in job_post_ids:
            writer.writerow({"id": job_post_id})


if __name__ == '__main__':
    path_to_csv_of_companies_i_have_applied_to = resource_filename(__name__, 'companies_i_have_applied_to.csv')
    path_to_csv_of_companies_i_am_going_to_apply_to = resource_filename(__name__, 'companies_i_am_going_to_apply_to.csv')
    path_to_csv_of_remoteok_job_posts_ive_processed = resource_filename(__name__, 'remoteok_job_posts_ive_processed.csv')
    get_and_save_companies_i_want_to_apply_to(path_to_csv_of_companies_i_have_applied_to,
                                              path_to_csv_of_companies_i_am_going_to_apply_to,
                                              path_to_csv_of_remoteok_job_posts_ive_processed)
