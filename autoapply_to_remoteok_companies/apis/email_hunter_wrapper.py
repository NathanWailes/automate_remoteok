"""
Problem: While debugging, I was using up a lot of my Email Hunter requests by repeatedly requesting info for the same
         companies.
Solution: I decided to permanently save the results of my Email Hunter requests. To do this, I'm wrapping the Email
          Hunter methods with an 'scached' decorator that will save queries and their responses to a '.db' file.
"""
import datetime
from email_hunter import EmailHunterClient
from autoapply_to_remoteok_companies.user_config import get_api_key
from autoapply_to_remoteok_companies.utils.cache_with_the_shelve_module import cache_with_the_shelve_module
from pkg_resources import resource_filename

db_folder_name = "email_hunter"


@cache_with_the_shelve_module(resource_filename("autoapply_to_remoteok_companies.apis", "db/" + db_folder_name + "/exist.db"), expiry=datetime.timedelta(weeks=52))
def exist(email_address):
    """
    :param email_address:
    :return:
    """
    client = EmailHunterClient(get_api_key("email_hunter"))
    response = client.exist(email_address)
    return response


@cache_with_the_shelve_module(db_folder_name + "search.db", expiry=datetime.timedelta(weeks=52))
def search(email_address, offset=0, type_=None):
    """
    :param email_address:
    :param _type:
    :return:
    """
    client = EmailHunterClient(get_api_key("email_hunter"))
    response = client.search(email_address, offset=offset, type_=type_)
    return response


if __name__ == '__main__':
    print(exist("nathan@infer.com"))
