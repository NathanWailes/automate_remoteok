import datetime

import logging
from googleapiclient.discovery import build


from autoapply_to_remoteok_companies.user_config import get_api_key, get_misc_info
from autoapply_to_remoteok_companies.utils.cache_with_the_shelve_module import cache_with_the_shelve_module
from pkg_resources import resource_filename

db_folder_name = "google"


def get_first_hundred_urls_when_searching_google_for(query):
    """
    :param query:
    :return:

    """
    result = get_first_hundred_results_when_searching_google_for(query)
    if not result.get('items'):
        logging.log(logging.INFO, "No items returned for query: %s" % query)
    return [item['formattedUrl'] for item in result.get('items', [])]


@cache_with_the_shelve_module(resource_filename("autoapply_to_remoteok_companies.apis", "db/" + db_folder_name + "/search.db"), expiry=datetime.timedelta(weeks=52))
def get_first_hundred_results_when_searching_google_for(query):
    """  I want to save the results of these queries in case I later want to go back and use the later search results.
    In other words, I don't just want to cache the FIRST url that Google responds with; I also want to save all of the
    other data they return.
    :param query:
    :return:

    """
    service = build("customsearch", "v1",
                    developerKey=get_api_key("google_search"))
    result = service.cse().list(
            q=query,
            cx=get_misc_info("google_custom_search_engine_id")).execute()
    return result
