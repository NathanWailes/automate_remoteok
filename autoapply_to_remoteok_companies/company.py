# TODO: Create a function that'll email you when the code hits an error.
import csv
import os
from autoapply_to_remoteok_companies.apis.google_search_wrapper import get_first_hundred_urls_when_searching_google_for

from difflib import SequenceMatcher
from collections import namedtuple
import logging
from autoapply_to_remoteok_companies.send_an_email import send_email_with_error_message
from tld import get_tld
from tld.exceptions import TldBadUrl

Company = namedtuple('Company', ['name', 'website_base_url', 'email'])


def get_companies_from_csv(path_to_csv_of_companies):
    """
    :param path_to_csv_of_companies:
    :return: A set of 'Company' namedtuples
    """
    with open(path_to_csv_of_companies, 'r') as infile:
        reader = csv.DictReader(infile)
        companies = set([Company(name=row['name'], website_base_url=row['website_base_url'], email=row['email'])
                         for row in reader])
    return companies


def save_company_to_csv(company, path_to_csv_to_store_results_in):
    """
    :param path_to_csv_to_store_results_in:
    :param company:
    :return:
    """
    logging.info(path_to_csv_to_store_results_in + ": Attempting to append company " + str(company))
    try:
        assert(not company_is_in_csv(company, path_to_csv_to_store_results_in))
    except AssertionError as e:
        e.args += (company)
        raise

    with open(path_to_csv_to_store_results_in, 'a') as outfile:
        fieldnames = ['name', 'website_base_url', 'email']
        writer = csv.DictWriter(outfile, lineterminator='\n', fieldnames=fieldnames)
        logging.info(path_to_csv_to_store_results_in + ": Appending company " + str(company))
        writer.writerow({'name': company.name, 'website_base_url': company.website_base_url, 'email': company.email})


def company_is_in_csv(company, path_to_csv):
    """
    :param company:
    :param path_to_csv:
    :return:
    """
    companies_in_the_csv = get_companies_from_csv(path_to_csv)
    return company_is_in_company_iterable(company, companies_in_the_csv)


def company_is_in_company_iterable(company, company_iterable):
    """ Background: I _really_ don't want to email companies more than once by mistake. One
    way this can happen is if two jobs show up on RemoteOK, each of which spells the company's
    name slightly differently, and the comparison I do to look for duplicates isn't able to
    pick up that the two jobs are for the same company.
        So, what I'm thinking is that I'll check each new company not just by its name, but also
    by its website_base_url.
    :param company:
    :param company_iterable:
    :return:
    """
    company_names = set(company.name for company in company_iterable)
    if company.name in company_names:
        return True

    company_websites = set(company.website_base_url for company in company_iterable)
    if company.website_base_url in company_websites:
        return True
    return False


def append_companies_to_csv(companies_i_want_to_add, path_to_csv_file):
    """
    :param companies_i_want_to_add:
    :param path_to_csv_file:
    :return:
    """
    companies_in_csv = get_companies_from_csv(path_to_csv_file)

    with open(path_to_csv_file, 'a') as outfile:
        fieldnames = ['name', 'website_base_url', 'email']
        writer = csv.DictWriter(outfile, lineterminator='\n', fieldnames=fieldnames)
        for company in companies_i_want_to_add:
            logging.info(path_to_csv_file + ": Attempting to append company " + str(company))
            try:
                assert(not company_is_in_company_iterable(company, companies_in_csv))  # Don't want to double-add a company
            except AssertionError as e:
                e.args += ("Company " + str(company) + " is already in CSV " + path_to_csv_file,)  # Must concatenate a tuple to e.args
                send_email_with_error_message("Company " + str(company) + " is already in CSV " + path_to_csv_file)
                raise

            writer.writerow({"name": company.name, "website_base_url": company.website_base_url, "email": company.email})
            companies_in_csv.add(company)


def overwrite_csv_with_companies(companies_i_want_to_save, path_to_csv_file):
    """ TODO: Name this function better, and maybe get rid of it if you can make the 'append' function mroe general.
    :param companies_i_want_to_save:
    :param path_to_csv_file:
    :return:
    """
    with open(path_to_csv_file, 'w') as outfile:
        fieldnames = ['name', 'website_base_url', 'email']
        writer = csv.DictWriter(outfile, lineterminator='\n', fieldnames=fieldnames)
        writer.writeheader()
        for company in companies_i_want_to_save:
            logging.info(path_to_csv_file + ": Writing company " + str(company))
            writer.writerow({"name": company.name, "website_base_url": company.website_base_url, "email": company.email})


def remove_company_from_csv(company, path_to_csv_file):
    """
    :param company:
    :param path_to_csv_file:
    :return:
    """
    logging.info(path_to_csv_file + ": Attempting to remove company " + str(company))
    try:
        assert(company_is_in_csv(company, path_to_csv_file))
    except AssertionError as e:
        e.args += ("Company " + str(company) + " is not in CSV " + path_to_csv_file,)  # Must concatenate a tuple to e.args
        send_email_with_error_message("Company " + str(company) + " is not in CSV " + path_to_csv_file)
    companies_in_csv = get_companies_from_csv(path_to_csv_file)
    original_number_of_companies = len(companies_in_csv)
    company_websites = set(company.website_base_url for company in companies_in_csv)
    if company.website_base_url in company_websites:
        companies_in_csv.remove(company)
    new_set_of_companies = companies_in_csv

    new_number_of_companies_in_csv = len(companies_in_csv)
    assert(new_number_of_companies_in_csv == original_number_of_companies - 1)
    os.remove(path_to_csv_file)
    overwrite_csv_with_companies(new_set_of_companies, path_to_csv_file)


def find_the_company_domain_on_google_given_the_name(company_name):
    """ Use the first search result, unless the similarity of the URL to the company's name is below 0.4; in that
    case, look through the rest of the search-result-URLs and take the best one (the URL that's most similar to
    the company name).

    >>> find_the_company_domain_on_google_given_the_name("Facebook")
    'facebook.com'

    >>> find_the_company_domain_on_google_given_the_name("serverless")
    'serverless.com'

    >>> find_the_company_domain_on_google_given_the_name("clarati")
    'clarati.co.uk'

    :param company_name:
    :return:
    """
    company_urls = get_first_hundred_urls_when_searching_google_for(company_name)
    company_domains = [_get_the_company_domain_given_its_url(url) for url in company_urls]
    company_domain_to_use = company_domains[0] if company_domains else None  # The top search result

    if not company_domain_to_use:
        return ''

    similarity_of_company_name_to_domain = _similarity_of_two_strings(company_name, company_domain_to_use)
    if similarity_of_company_name_to_domain < 0.4:  # The threshold is based on eyeballing some examples.
        max_similarity = similarity_of_company_name_to_domain
        for company_domain in company_domains:
            similarity = _similarity_of_two_strings(company_name, company_domain)
            if similarity > max_similarity:
                company_domain_to_use = company_domain
                max_similarity = similarity

    return company_domain_to_use


def _similarity_of_two_strings(string_1, string_2):
    """
    :param s1:
    :param s2:
    :return:
    """
    if string_1 is None:
        string_1 = ''
    if string_2 is None:
        string_2= ''
    return SequenceMatcher(None, string_1, string_2).ratio()


def _get_the_company_domain_given_its_url(url_of_company):
    """
    Confirmed edge cases to handle:
    www.thesixgill.com/ needs to be transformed to http://www.thesixgill.com
    nationbuilder.com/ needs to be transformed to http://www.nationbuilder.com
    :param url_of_company: Ex: "http://www.google.com"
    :return: Ex: 'google.com'
    """
    try:
        company_domain = get_tld(url_of_company)
    except TldBadUrl:
        if not (url_of_company.startswith("http://") or url_of_company.startswith("https://")):  # Ex: "www.thesixgill.com/"
            company_domain = get_tld("http://" + url_of_company)
        else:
            raise
    try:
        assert(company_domain and isinstance(company_domain, str) and "." in company_domain and len(company_domain) > 3)
    except AssertionError as e:
        e.args += ("Invalid company domain", company_domain)
        raise
    return company_domain
