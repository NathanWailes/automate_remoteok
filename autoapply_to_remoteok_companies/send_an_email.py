# This code was built off the answers to this SO question: http://stackoverflow.com/questions/3362600/
import os
import smtplib
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from autoapply_to_remoteok_companies.user_config import get_email_settings


def send_email_with_error_message(error_message):
    """
    :param error_message:
    :return:
    """
    email_settings = get_email_settings()

    send_an_email(email_settings['username'], email_settings['error_email_recipients'], subject="Error",
                  text=error_message, server=email_settings['server'], port=email_settings['port'],
                  username=email_settings['username'], password=email_settings['password'],
                  is_tls=email_settings['is_tls'])
    return


def send_an_email(sender_email_address, list_of_to_recipient_email_addresses, list_of_cc_recipient_email_addresses=None,
                  list_of_bcc_recipient_email_addresses=None,
                  subject=None, text=None, list_of_paths_to_attachments=None, server="localhost", port=587, username='',
                  password='', is_tls=True):
    """
    :param sender_email_address:
    :param list_of_to_recipient_email_addresses: This is EVERYONE getting the email, including those CC'd and BCC'd.
    :param list_of_cc_recipient_email_addresses:
    :param list_of_bcc_recipient_email_addresses:
    :param subject:
    :param text:
    :param list_of_paths_to_attachments:
    :param server:
    :param port:
    :param username:
    :param password:
    :param is_tls:
    :return:
    """
    message = _get_mime_message(sender_email_address, list_of_to_recipient_email_addresses,
                                list_of_cc_recipient_email_addresses, subject, text, list_of_paths_to_attachments)

    smtp = smtplib.SMTP(server, port)
    if is_tls:
        smtp.starttls()
    smtp.login(username, password)

    list_of_all_recipient_email_addresses = _get_list_of_all_recipient_email_addresses(
            list_of_to_recipient_email_addresses, list_of_cc_recipient_email_addresses,
            list_of_bcc_recipient_email_addresses)

    smtp.sendmail(sender_email_address, list_of_all_recipient_email_addresses, message.as_string())
    smtp.quit()


def _get_mime_message(sender_email_address, list_of_to_recipient_email_addresses, list_of_cc_recipient_email_addresses=None,
                      subject=None, text=None, list_of_paths_to_attachments=None):
    """
    :param sender_email_address:
    :param list_of_to_recipient_email_addresses:
    :param list_of_cc_recipient_email_addresses:
    :param subject:
    :param text:
    :param list_of_paths_to_attachments:
    :return:
    """
    message = MIMEMultipart()
    message['From'] = sender_email_address

    message['To'] = COMMASPACE.join(list_of_to_recipient_email_addresses)
    if list_of_cc_recipient_email_addresses is not None:
        message['CC'] = COMMASPACE.join(list_of_cc_recipient_email_addresses)

    message['Date'] = formatdate(localtime=True)

    if subject is None:
        subject = ""
    message['Subject'] = subject

    if text is None:
        text = ""
    message.attach(MIMEText(text))

    if list_of_paths_to_attachments is None:
        list_of_paths_to_attachments = []
    for path_to_attachment in list_of_paths_to_attachments:
        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(path_to_attachment, "rb").read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="{0}"'.format(os.path.basename(path_to_attachment)))
        message.attach(part)
    return message


def _get_list_of_all_recipient_email_addresses(list_of_to_recipient_email_addresses,
                                               list_of_cc_recipient_email_addresses,
                                               list_of_bcc_recipient_email_addresses):
    """ I suspect there's a way to shorten this function...
    :param list_of_to_recipient_email_addresses:
    :param list_of_cc_recipient_email_addresses:
    :param list_of_bcc_recipient_email_addresses:
    :return:
    """
    assert(isinstance(list_of_to_recipient_email_addresses, list) and len(list_of_to_recipient_email_addresses) > 0)

    if list_of_cc_recipient_email_addresses is None:
        list_of_cc_recipient_email_addresses = []
    if list_of_bcc_recipient_email_addresses is None:
        list_of_bcc_recipient_email_addresses = []

    list_of_all_recipient_email_addresses = list_of_to_recipient_email_addresses + \
        list_of_cc_recipient_email_addresses + list_of_bcc_recipient_email_addresses

    return list_of_all_recipient_email_addresses


if __name__ == '__main__':
    import doctest
    doctest.testmod()
