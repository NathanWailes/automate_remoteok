""" I probably don't need these as three different functions...
...on the other hand, this makes it easier for me to change the structure of the JSON.
"""
import json
from pkg_resources import resource_filename


def get_email_settings():
    """
    :return:
    """
    user_config = _get_user_config()
    return user_config['email_settings']


def get_api_key(key_name):
    """
    :param key_name:
    :return:
    """
    user_config = _get_user_config()
    api_keys = user_config['api_keys']
    return api_keys[key_name]


def get_misc_info(info_name):
    """
    :param info_name:
    :return:
    """
    user_config = _get_user_config()
    misc_info = user_config['misc_info']
    return misc_info[info_name]


def _get_user_config():
    with open(resource_filename(__name__, 'user_config.json'), 'r') as infile:
        user_config = json.load(infile)
    return user_config
