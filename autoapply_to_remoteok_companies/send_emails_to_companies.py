""" Original full name of this file: send_emails_to_companies_that_have_posted_a_remote_job
TODOs:
TODO: Save the email addresses / companies to a CSV before you email them, and email yourself once a day with a list
of the emails you're going to send out, so you have time to fix problems.

Important objects in this module:
Company - A namedtuple with a 'name' field and a 'website' field.
"""
import time
from collections import namedtuple
from pkg_resources import resource_filename
from autoapply_to_remoteok_companies.send_an_email import send_an_email, send_email_with_error_message
from autoapply_to_remoteok_companies.company import get_companies_from_csv, save_company_to_csv, remove_company_from_csv, \
    company_is_in_csv
from autoapply_to_remoteok_companies.user_config import get_email_settings
from autoapply_to_remoteok_companies.utils import run_with_logging
import logging
import sys

# The line below is to avoid creating .pyc files, which can get 'stale': you can update a repository, run the code, and
# it will use the .pyc file, which will still be using the old version of the code, instead of the .py file, which will
# have any new changes you've made.
sys.dont_write_bytecode = True

EmailTemplate = namedtuple("EmailTemplate", "sender_email_address username password subject message "
                                            "list_of_to_recipient_email_addresses list_of_cc_recipient_email_addresses "
                                            "list_of_bcc_recipient_email_addresses list_of_paths_to_attachments "
                                            "server port is_tls")


def main():
    logging.info("Getting email settings")
    email_settings = get_email_settings()

    email_template = EmailTemplate(
        username=email_settings['username'],
        password=email_settings['password'],
        sender_email_address=email_settings['username'],
        message="".join(email_settings['message']),
        list_of_to_recipient_email_addresses=email_settings['list_of_to_recipient_email_addresses'],
        list_of_cc_recipient_email_addresses=email_settings['list_of_cc_recipient_email_addresses'],
        list_of_bcc_recipient_email_addresses=email_settings['list_of_bcc_recipient_email_addresses'],
        subject=email_settings['subject'],
        list_of_paths_to_attachments=[resource_filename("autoapply_to_remoteok_companies", "attachments/" + attachment_name)
                                      for attachment_name in email_settings['list_of_attachments']],
        server=email_settings['server'],
        port=email_settings['port'],
        is_tls=email_settings['is_tls']
    )

    path_to_csv_with_companies_i_am_going_to_apply_to = resource_filename("autoapply_to_remoteok_companies",
                                                                          "companies_i_am_going_to_apply_to.csv")
    path_to_csv_with_companies_i_have_applied_to = resource_filename("autoapply_to_remoteok_companies",
                                                                     "companies_i_have_applied_to.csv")
    send_emails_to_companies_listed_in_a_csv(email_template, path_to_csv_with_companies_i_am_going_to_apply_to,
                                             path_to_csv_with_companies_i_have_applied_to)


@run_with_logging
def send_emails_to_companies_listed_in_a_csv(email_template, path_to_csv_of_companies_i_am_going_to_apply_to,
                                             path_to_csv_of_companies_i_have_applied_to):
    """ Thought: I need to have this function only email jobs that have been in the CSV for a day, otherwise I could have
    a company added to the CSV in the time between when I get the email and when the emails are sent...
    :param email_template:
    :param path_to_csv_of_companies_i_am_going_to_apply_to:
    :param path_to_csv_of_companies_i_have_applied_to:
    :return:
    """
    logging.info("Getting companies I am going to apply to.")
    companies_i_am_going_to_apply_to = get_companies_from_csv(path_to_csv_of_companies_i_am_going_to_apply_to)

    for company in list(companies_i_am_going_to_apply_to)[:30]:  # Only do 30 companies at a time to avoid getting stopped by Google.
        logging.info("Now handling " + str(company))
        assert(company.name.lower() not in ['infer', 'remoteok'] and
               company.website_base_url.lower() not in ['infer.com', 'remoteok.io'])

        try:
            assert(not company_is_in_csv(company, path_to_csv_of_companies_i_have_applied_to))  # Don't want to double-add a company
        except AssertionError as e:  # TODO: Clean up this code, it's messy to have all of this in this function.
            error_message = "Company " + str(company) + " is already in CSV " + path_to_csv_of_companies_i_have_applied_to
            logging.info(error_message)
            e.args += (error_message,)  # Must concatenate a tuple to e.args
            send_email_with_error_message(error_message)
            raise
        _send_email_to_company_that_has_posted_a_job_on_remoteok(email_template, company)
        save_company_to_csv(company, path_to_csv_of_companies_i_have_applied_to)
        remove_company_from_csv(company, path_to_csv_of_companies_i_am_going_to_apply_to)

        time.sleep(10)


def _send_email_to_company_that_has_posted_a_job_on_remoteok(email_template, company):
    """
    :param email_template:
    :param company: A namedtuple, see company.py for its definition.
    :return:
    """
    logging.info("Now emailing: " + company.email)

    send_an_email(username=email_template.username,
                  password=email_template.password,
                  sender_email_address=email_template.sender_email_address,
                  list_of_to_recipient_email_addresses=email_template.list_of_to_recipient_email_addresses + [company.email],
                  list_of_cc_recipient_email_addresses=email_template.list_of_cc_recipient_email_addresses,
                  list_of_bcc_recipient_email_addresses=email_template.list_of_bcc_recipient_email_addresses,
                  subject=email_template.subject,
                  text=email_template.message,
                  list_of_paths_to_attachments=email_template.list_of_paths_to_attachments,
                  server=email_template.server,
                  port=email_template.port,
                  is_tls=email_template.is_tls)


if __name__ == '__main__':
    #import doctest
    #doctest.testmod()
    main()
