#!/bin/bash
source /home/nathanwailes/.virtualenvs/automate_stuff/bin/activate
cd /home/nathanwailes/automate/automate_remoteok/
python -m autoapply_to_remoteok_companies.send_emails_to_companies_that_have_posted_a_remote_job
deactivate
